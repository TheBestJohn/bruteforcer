GPS Bruteforcer
===============

A Simple MD5 Bruteforce script


## About

This python script is used to brute force a geocache location that has been obscured with the MD5 hashing algorithm. To use, structure your test case into the format provided by the cache owner. Then change the hash variable to the one provided.

Next run in the command line using the following:

	cd ~/bruteforcer
	python force.py

in a few minutes, if everything is set up properly and the hash is good, it should pop out the co-ords of the hash.